FROM php:7.4-fpm

RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    libmagickwand-dev \
    ghostscript \
    libpq-dev \
    libzip-dev

RUN pecl install redis zip

RUN docker-php-ext-install gd pcntl bcmath sockets
RUN docker-php-ext-enable zip

ADD .docker/local/config/ini/memory-limit.ini /usr/local/etc/php/conf.d/memory-limit.ini

# Sort out user permissions on ubuntu
RUN useradd gometro
RUN mkdir -p /home/gometro/.config
RUN chown -R gometro:gometro /home/gometro/.config
RUN chmod -R 775 /home/gometro/.config

COPY --from=composer:1.10.9 /usr/bin/composer /usr/bin/composer
