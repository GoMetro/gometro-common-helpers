<?php

namespace Tests\Csv;

use GoMetroCommonHelpers\Csv\CsvToAssociativeArray;
use PHPUnit\Framework\TestCase;

class CsvToAssociativeArrayTest extends TestCase
{
    public function testReturnsAssociativeArray()
    {
        $result = CsvToAssociativeArray::run(
            'col1,col2'.PHP_EOL
            .'value1,value2'.PHP_EOL,
        );

        $this->assertEquals(
            [[
                'col1' => 'value1',
                'col2' => 'value2',
            ]],
            $result
        );
    }
}
