<?php

namespace GoMetroCommonHelpers\Csv;

class CsvToAssociativeArray
{
    public static function run(string $csv_seed, string $delimiter = ','): array
    {
        $csv = explode(PHP_EOL, $csv_seed);
        $rows = array_map(fn ($row) => str_getcsv($row, $delimiter), $csv);
        $header = array_shift($rows);
        $associated_csv = [];

        foreach ($rows as $row) {
            if (empty($row) || count($row) <= 1) {
                continue;
            }

            $associated_csv[] = array_combine($header, $row);
        }

        return $associated_csv;
    }
}
